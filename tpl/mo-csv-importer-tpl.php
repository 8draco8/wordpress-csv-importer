<link rel="stylesheet" type="text/css" href="<?php echo plugins_url( '../css/custom.css' , __FILE__ ); ?>">
<h2>Media Orb CSV Importer</h2>
<div class="mo-csv-importer">
  <form name="loadPreview" method="POST" enctype="multipart/form-data">
    <label for="file" class="cabinet">
      <input type="file" name="file" id="file" accept="text/csv" class="file" /><sup>Max file size: 8MB</sup>
    </label>
    <input type="submit" name="submit" value="Submit">    
  </form>
  <hr>
  <div class="informations">
  <h3>Instructions for CSV file:</h3>
    <p>
      <ul>
        <li>Headers have to match list below (all columns are required):
          <ul>
            <li>ID</li>
            <li>YearNo</li>
            <li>Make</li>
            <li>Model</li>
            <li>Trim</li>
            <li>Engcc</li>
            <li>FuelType</li>
            <li>Body</li>
            <li>Colour</li>
            <li>Doors</li>
            <li>Gearbox</li>
            <li>REGNO</li>
            <li>Mileage</li>
            <li>Pout</li>
          </ul>
        </li>
        <li>ID column has to be empty.</li>
        <li>Registration Number has to be unique.</li>
        <li>Field delimiter has to be semicolon (;), comma will not work.</li>
      </ul>  
    </p>
    <p><a href="<?php echo plugins_url( '../example.csv' , __FILE__ ); ?>">Example File (click to download)</p>
  </div>
</div>