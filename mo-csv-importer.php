<?php
/*
 * @wordpress-plugin
 * Plugin Name:       Media Orb CSV Importer
 * Plugin URI:        http://mediaorb.co.uk/
 * Description:       This plugin allows import CSVs directly in to database
 * Version:           0.1
 * Author:            Andrzej Jarzebowski
 * Author URI:        http://mediaorb.co.uk
 * Text Domain:       mo-csv-importer
 * License:           MIT
 * License URI:       http://opensource.org/licenses/MIT
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
  die;
}

add_action( 'admin_menu', 'mo_csv_importer_menu' );

// WP function to add link to menu http://codex.wordpress.org/Function_Reference/add_menu_page
function mo_csv_importer_menu() {
  add_menu_page( 'MO CSV Importer Options', 'CSV Importer', 'manage_options', 'mo-csv-importer', 'mo_csv_importer_options', 'http://media-orb.co.uk/images/favicon.png', '0' );
}

// Converting CSV to array
function csv_to_array($filename='', $delimiter=';')
{
  if(!file_exists($filename) || !is_readable($filename))
    return FALSE;  
  $header = NULL;
  $data = array();
  if (($handle = fopen($filename, 'r')) !== FALSE)
  {
    while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
    {
      if(!$header)
        $header = $row;
      else
        $data[] = array_combine($header, $row);
    }
    fclose($handle);
  }
  return $data;
}

// Changes all post of type 'vehicles' to draft status
function mo_turn_off_all_cars($link) {
  $query = "UPDATE `wp_posts` 
            SET `post_status`= 'draft' 
            WHERE `post_type`= 'vehicles' ";
  $result = $link->query($query);
  return $result;
}

// Changes all post of given $id to publish status
function mo_turn_on_cars($id, $link) {
  if (isset($id)) {
    $query = "UPDATE `wp_posts` 
              SET wp_posts.post_status = 'publish' 
              WHERE wp_posts.ID = ".$id;
    $result = $link->query($query);
    return $result;
  }
}

// Finds ids of all cars by looking for wp_postmeta with 'registration_no' meta_key
function mo_get_cars_id($link) 
{
  $query = "SELECT * FROM `wp_postmeta`"
           . "WHERE wp_postmeta.meta_key = 'registration_no'";
  
  if (($result = $link->query($query))===false) {
    printf("Invalid query: %s\nWhole query: %s\n", $mysqli->error, $query);
    die();
  }
               
  return $result;
}

// Main CSV importer function
function mo_csv_importer_options() {
  include( dirname( dirname ( __FILE__ ) ) ."/../../wp-config.php"); 
  $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die("Error " . mysqli_error($link));
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }
  require_once( plugin_dir_path( __FILE__ ) . 'tpl/mo-csv-importer-tpl.php' );

// File upload start
  $upload_link = wp_upload_dir();
  $tmp_link = $upload_link['path'];
  if ( isset($_POST["submit"]) ) {
   if ( isset($_FILES["file"])) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        }
        else {
          // Print out some info about uploaded file
          echo "Upload: " . $_FILES["file"]["name"] . "<br />";
          echo "Type: " . $_FILES["file"]["type"] . "<br />";
          echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
          
          if (file_exists($tmp_link . "/" . $_FILES["file"]["name"])) {
            echo $_FILES["file"]["name"] . " already exists. ";
          } else {
            $length = 3;
            $string = "";
            
            while ($length > 0) {
                $string .= dechex(mt_rand(0,15));
                $length -= 1;
            }
            
            // Every uploaded file has different name
            $storagename = 'upload-'.$string.'-'.date('Y-m-d');  
            move_uploaded_file($_FILES["file"]["tmp_name"], $tmp_link . '/' . $storagename . '.csv');
            $file_link = $tmp_link.'/'.$storagename.'.csv';

            mo_turn_off_all_cars($link); // Turns off all cars
            
            $file_array = csv_to_array($file_link,';'); // Putting uploaded file into array
            foreach ($file_array as $key) {
              $regno = $key['REGNO']; 
              $cars_id = mo_get_cars_id($link); // Getting all ids of cars
              while ($row = $cars_id->fetch_array(MYSQLI_ASSOC)) {
                if ($row['meta_value'] == $regno) { // Checks if car is already in db by looking for registration number
                  $current_car_id = $row['post_id']; 
                  $key['ID'] = $current_car_id; // Adds id to ID in array created from CSV file
                                                // that preevents from duplicating cars
                }
              }
              $id = $key['ID'];
              mo_turn_on_cars($id, $link); // Turning on cars that already in db

              // Adding or updating cars http://codex.wordpress.org/Function_Reference/wp_insert_post
              $name = $key['Make'].' '.$key['Model'].' '.$key['Trim'].' '.$key['Engcc'].' '.$key['YearNo'];
              $post = array(
                'ID'             => $key['ID'],
                'post_content'   => ' ',
                'post_title'     => $name,
                'post_status'    => 'publish',
                'post_type'      => 'vehicles',
                'post_author'    => '1',
                'ping_status'    => 'closed',
                'post_parent'    => '0',
                'comment_status' => 'closed'
              );  
              $post_id = wp_insert_post($post);

              // Changes car age to specific format ('X years old')
              $current_year = date('Y');
              $years_old = intval($current_year) - intval($key['YearNo']);
              $years_old_txt = $years_old.' Years Old';

              // Adding or updating taxonomy and post meta of created car 
              // http://codex.wordpress.org/Function_Reference/wp_set_object_terms
              // http://codex.wordpress.org/Function_Reference/add_post_meta
              // http://codex.wordpress.org/Function_Reference/update_post_meta
              wp_set_object_terms( $post_id, $key['Colour'], 'vehicle_color' );
              wp_set_object_terms( $post_id, $key['Body'], 'vehicle_type' );
              wp_set_object_terms( $post_id, $key['FuelType'], 'vehicle_fuel_type' );
              wp_set_object_terms( $post_id, 'used', 'vehicle_status' );
              wp_set_object_terms( $post_id, $key['Gearbox'], 'vehicle_gearbox' );
              wp_set_object_terms( $post_id, $years_old_txt, 'vehicle_year' );
              wp_set_object_terms( $post_id, $key['Model'], 'vehicle_model' );
              // wp_set_object_terms( $post_id, $key['Make'], 'vehicle_model' );
              add_post_meta($post_id, 'price', $key['Pout'], true) || update_post_meta($post_id, 'price', $key['Pout']);
              add_post_meta($post_id, 'mileage', $key['Mileage'], true) || update_post_meta($post_id, 'mileage', $key['Mileage']);
              add_post_meta($post_id, 'capacity', $key['Engcc'], true) || update_post_meta($post_id, 'capacity', $key['Engcc']);
              add_post_meta($post_id, 'doors', $key['Doors'], true) || update_post_meta($post_id, 'doors', $key['Doors']);
              add_post_meta($post_id, 'registration_no', $key['REGNO'], true) || update_post_meta($post_id, 'registration_no', $key['REGNO']);       
            }
        }
      }
        echo "<br>Vehicles has been updated<br><hr>";
     } else { 
             echo "No file selected <br />";
     } 
  }
} 